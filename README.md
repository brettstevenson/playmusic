![PlayMusic](https://user-images.githubusercontent.com/16360374/51782448-50fb0480-20dd-11e9-8e53-82698c74957d.png)  
----------------------------------------------------------------------------  

![Version](https://badge.fury.io/gh/tterb%2FPlayMusic.svg)
![License](https://img.shields.io/badge/license-AGPL-blue.svg)
![Issues](https://img.shields.io/github/issues-raw/tterb/PlayMusic.svg?maxAge=25000)
![Downloads](https://img.shields.io/badge/downloads-%208%2C130-green.svg)  


![Preview](https://cloud.githubusercontent.com/assets/16360374/22547134/1a4e0da0-e8f5-11e6-9802-08228a312ec6.gif)  


# Description  

  This [Rainmeter] skin provides [Google Play Music] users the ability to access their active media tastefully on their desktop. Featuring three variants, this skin displays the Album Artwork accompanied by the Title, Artist, and Album of the song being played, as well as the status of both the *shuffle* and *repeat* features.
  Additionally, this skin also includes a variety of supplementary features including a customizable visualizer, scalability, as well as volume control and minimization functionalities.

#### In Progress:
  * Add requested support for **[Spotify]** as well as other media players
  * Improve accent color adaptability for all album artwork
  * Add performance settings and further customization to visualizer  
  
  
------------------------------------------------------------------------------  

# Features  

![Variants](https://user-images.githubusercontent.com/16360374/51782518-8fdd8a00-20de-11e9-8e65-d59a90e84392.png)  


### Variants
PlayMusic features square, round, and landscape variants, which in conjunction with its added scalability settings, guarantee its ability to compliment any desktop layout.  
</p><br>

### Settings  
In order to facilitate easy customization, PlayMusic includes a highly intuitive settings menu, making configuration and modification a breeze. This menu will be launched upon installation, though it can also be accessed at any time by pressing the &nbsp;<img src="https://cloud.githubusercontent.com/assets/16360374/21559428/fe6a3654-ce00-11e6-8d2c-ba8570e3132c.png" height="12">&nbsp; on any of the skin variants.  

### Accent Colors  
The addition of selectable accent colors allows users to add a splash of color to their skin, while also allowing PlayMusic to tastefully adapt to any theme, layout, or wallpaper.  

### Visualizer  
In addition to providing added functionality to the skin, the recent adoption of a highly customizable visualizer also allows one more way of making this skin your own.  

### Volume control  
This skin also features the ability to easily adjust your computers volume by simply <br>  scrolling up or down on the mouse-wheel with your cursor over the skin.  

------------------------------------------------------------------------------  

# Setup

This skin requires you have **[Google Play Music Desktop Player]** installed to function properly. After installation, you must also enable **Playback API** in GPMDP's *Desktop Settings*, shown in the image below, as this allows GPMDP to interface with external applications.  

![Settings](https://user-images.githubusercontent.com/16360374/51782531-ce734480-20de-11e9-8304-513a8fa791d0.png)  

------------------------------------------------------------------------------  

# Documentation  

  * [License](docs/LICENSE.md)
  * [Changelog](docs/CHANGELOG.md)
  * [Contributing](docs/CONTRIBUTING.md)
  * [Credits](docs/CREDITS.md)  
  
  

[Rainmeter]: https://www.rainmeter.net/
[Google Play Music]: https://play.google.com/music
[Google Play Music Desktop Player]: http://www.googleplaymusicdesktopplayer.com/
[Spotify]: https://www.spotify.com/
